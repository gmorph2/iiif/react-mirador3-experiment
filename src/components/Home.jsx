import React from 'react'
import Grid from '@material-ui/core/Grid'

import Mirador from 'containers/Mirador'

export default class Home extends React.Component {
  render() {
    return (
      <Grid container spacing={16} style={{height: '100%'}}>
        <Grid item xs={3}>
          <h1>App</h1>
        </Grid>
        <Grid item xs={9}>
          <Mirador/>
        </Grid>
      </Grid>
    )
  }
}
