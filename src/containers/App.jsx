import React from 'react'
import { createStore } from 'redux'
import { connect, Provider } from 'react-redux'

import rootReducer from 'reducers'

/*
import {
  updateServerState
} from 'src/actions'
*/

import Home from '../components/Home'

const mapStateToProps = state => state

const mapDispatchToProps = dispatch => {
  return {
    /*
    updateServerState: (serverState) => {
      dispatch(updateServerState(serverState))
    }
    */
  }
}

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentWillMount() {
    const store = createStore(rootReducer)
    this.setState({ store: store })
  }

  render() {
    const Connected = connect(mapStateToProps, mapDispatchToProps)(Home)
    return (
      <Provider store={this.state.store}>
        <Connected/>
      </Provider>
    )
  }
}

export default App
